var _ = require( 'lodash' );
var providers = require( 'omnibug/common/providers' );
var casper = require('casper').create();
var tagPixels = [];
var omniTags = [];

//_.forEach( providers, function ( provider ) {
//    console.log( provider );
//});

casper.on( 'resource.requested', function ( requestData, networkRequest ) {
    var tag = providers.getProviderForUrl( requestData.url);

    if ( tag.name !== 'Unknown' ) {
        console.log( tag.name );
        omniTags.push( tag );
    }

    if ( _.includes( requestData.url, 'rei/main/prod/utag.js' ) ) {
        console.log( 'Tealium' );
        tagPixels.push( requestData.url );
    }

    // Pattern from, https://github.com/simpsora/omnibug/blob/master/common/providers.js
    if ( /\/b\/ss\/|2o7/.test( requestData.url ) ) {
        tagPixels.push( requestData.url );
        networkRequest.abort();
    }
    if ( _.includes( requestData.url, 'log.dmtry.com' ) ) {
        tagPixels.push( requestData.url );
        networkRequest.abort();
    }
});

// TODO Product page tags are not consistantly captured, http://www.rei.com/product/873197/hoka-one-one-clifton-road-running-shoes-mens
casper.start('http://www.rei.com/product/873197/hoka-one-one-clifton-road-running-shoes-mens' );
casper.then( casper.wait.bind( casper, 5000, function() {
    console.log( 'Found ' + tagPixels.length + ' tags \n' );
    tagPixels.forEach( function ( url, num ) {
        console.log( num + 1 + '\t' + url + '\n' );
    });
}) );

casper.run();
