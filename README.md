# CasperJS Test
This is a simple project to test out the different functionalities of
[CasperJS](http://casperjs.org/) (PhantomJS) to test 3rd party scripts and an analytics implementation.

## Tasks

* Identify tags
    * Build a module to identify third-party requests
* Track Page Load Tags
    * Build a module to capture all the tags made from the initial load of a page.
* Track Page Interactions Tags
    * Capture all the third-party request made after the page has loaded (click, hover)

## Resources
* [CasperJS Docs](http://docs.casperjs.org/en/latest/)
* [Omnibug](https://github.com/simpsora/omnibug/issues)
